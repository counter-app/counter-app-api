Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  scope '/api' do
    get '/counter',       to: 'api#get', as: 'get_counter'
    post '/counter/inc',   to: 'api#inc', as: 'inc_counter'
    post '/counter/dec',   to: 'api#dec', as: 'dec_counter'
  end
end
