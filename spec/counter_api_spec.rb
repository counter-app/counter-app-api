require 'rails_helper'

RSpec.describe ApiController, type: :controller do
  describe "GET #get" do
    before do
      get :get
    end
    it "should return success http" do
      expect(response).to have_http_status(:success)
    end    
    it "should return counter item" do
      hash_body = nil
      expect { hash_body = JSON.parse(response.body).with_indifferent_access }.not_to raise_exception
      expect(hash_body.keys).to match_array(["points"])
    end
  end

  describe "POST #inc" do
    def get_counter
      get :get
    end

    def inc_points
      post :inc
    end

    it "should correctly increase points" do
      get_counter
      hash_body = JSON.parse(response.body).with_indifferent_access
      expect(hash_body.keys).to match_array(["points"])
      init_value = hash_body[:points].to_i
      inc_points
      expect { hash_body = JSON.parse(response.body).with_indifferent_access }.not_to raise_exception
      expect(hash_body.keys).to match_array(["counter", "status"])
      expect(init_value+1).to eq(hash_body[:counter][:points])
    end
  end

  describe "POST #dec" do
    def get_counter
      get :get
    end

    def dec_points
      post :dec
    end

    it "should correctly decrease points" do
      get_counter
      hash_body = JSON.parse(response.body).with_indifferent_access
      expect(hash_body.keys).to match_array(["points"])
      init_value = hash_body[:points].to_i
      dec_points
      expect { hash_body = JSON.parse(response.body).with_indifferent_access }.not_to raise_exception
      expect(hash_body.keys).to match_array(["counter", "status"])
      expect(init_value-1).to eq(hash_body[:counter][:points])
    end
  end

end
