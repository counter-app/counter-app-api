class Counter
  include Mongoid::Document

  field :points, type: Integer

  def inc!
    self.points += 1
    self.save
  end

  def dec!
    self.points -= 1
    self.save
  end

  def humanized
    {
      points: self.points
    }
  end
end
