class ApiController < ApplicationController
  before_action :set_counter

  def get
    render json: @counter.humanized
    # if @apple.save
    #   render json: @apple, status: :created, location: @apple
    # else
    #   render json: @apple.errors, status: :unprocessable_entity
    # end
  end

  def inc
    if @counter.inc!
      render json: { counter: @counter.humanized, status: 1 }
    else
      render json: { counter: @counter.errors, status: 0 }
    end
  end

  def dec
    if @counter.dec!
      render json: { counter: @counter.humanized, status: 1 }
    else
      render json: { counter: @counter.errors, status: 0 }
    end
  end

  private

  def set_counter
    @counter = Counter.first
  end
end
